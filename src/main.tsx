import renderer, { tsx, create } from '@dojo/framework/core/vdom';
import icache from '@dojo/framework/core/middleware/icache';

const factory = create({ icache });

const App = factory(function testAPI({ middleware: { icache } }) {
    icache.getOrSet('response', async () => {
        let resp = await fetch('api');
        return resp.text();
    });

    return (<div>
        <p>Making an api request...</p>
        <p>{icache.get('response') || 'Awaiting api response'}</p>
    </div>
    );
});

const r = renderer(() => <App />);
r.mount();
