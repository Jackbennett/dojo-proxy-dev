const http = require('http')

const [host, port] = ['0.0.0.0', 9998]

const server = http.createServer((req, res) => {
    console.log(`Request: ${req.method} to ${req.url}`)
    res.statusCode = 200
    res.setHeader('Content-Type', 'Application/JSON')
    res.end(JSON.stringify({
        name: 'api response'
    }))
})

server.listen(9998, '0.0.0.0', () => console.log(`listening on ${host}:${port}`))